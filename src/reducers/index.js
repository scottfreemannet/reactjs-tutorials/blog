import { combineReducers } from "redux";
import postsRedcuer from "./postsReducer";
import usersReducer from "./usersReducer";

export default combineReducers({
  posts: postsRedcuer,
  users: usersReducer,
});
